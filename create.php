<?php
require 'framework/create/run.php';

// 准备创建项目
$path = (isset($argv[1]) && $argv[1]) ? $argv[1] : exit('please type path');
$projectName = (isset($argv[2]) && $argv[2]) ? $argv[2] : exit('please type project name');
$startFile = (isset($argv[3]) && $argv[3]) ? $argv[3] : false;
new Create($startFile, $path, $projectName);
