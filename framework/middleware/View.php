<?php

namespace top\middleware;

use top\library\Cache;
use top\library\http\Request;
use top\library\http\Response;
use top\middleware\ifs\MiddlewareIfs;

/**
 * 检查是否存在静态缓存
 * Class View
 * @package top\middleware
 */
class View implements MiddlewareIfs
{

    public function handler(Request $request, \Closure $next)
    {
        // 非调试模式则直接返回静态缓存
        if (!DEBUG) {
            $ident = view_cache_ident();
            $config = \config('view');
            (!$config['cache_path']) && $config['cache_path'] = './runtime/cache/application/' . request()->module() . '/';
            $cache = Cache::instance($config['cache_path']);
            if ($cache->exists($ident)) return Response::instance()->send($cache->get($ident));
        }

        return $next();
    }

}
