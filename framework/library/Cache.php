<?php

namespace top\library;

use top\library\cache\driver\FileCache;
use top\library\cache\driver\RedisCache;
use top\traits\Instance;

/**
 * 缓存
 * top\library\cache\Cache
 */
class Cache
{
    use Instance;

    /**
     * 当前驱动
     *
     * @var top\library\cache\ifs\CacheIfs
     */
    private $driver;

    /**
     * 构造函数
     * Redis constructor.
     */
    private function __construct()
    {
        $config = config('cache');
        $type = strtolower($config['driver']);
        switch ($type) {
            case 'file':
                $path = '';
                if ($config['path']) {
                    $path = $config['path'];
                }
                $this->driver = FileCache::instance($path);
                break;
            case 'redis':
                $this->driver = RedisCache::instance();
                break;
            default:
                break;
        }
    }

    /**
     * 设置缓存
     * @param string $key
     * @param mixed $value
     * @param int $expire
     * @return bool
     */
    public function set($key, $value, $expire = 0)
    {
        return $this->driver->set($key, $value, $expire);
    }

    /**
     * 获取缓存
     * @param string $key
     * @return mixed
     */
    public function get($key)
    {
        return $this->driver->get($key);
    }

    /**
     * 移除缓存
     * @param string $key
     * @return bool
     */
    public function remove($key)
    {
        return $this->driver->remove($key);
    }

    /**
     * 判断是否存在
     * @param string $key
     * @return bool
     */
    public function exists($key)
    {
        return $this->driver->exists($key);
    }
    
}
