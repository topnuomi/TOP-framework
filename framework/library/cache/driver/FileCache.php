<?php

namespace top\library\cache\driver;

use top\library\cache\ifs\CacheIfs;
use top\traits\Instance;

/**
 * 文件缓存
 * Class FileCache
 * @package top\library\cache
 */
class FileCache implements CacheIfs
{

    use Instance;

    /**
     * 当前实例
     */
    private static $instance;

    /**
     * 已读取的文件
     * @var array
     */
    private $files = [];

    /**
     * 默认缓存位置
     * @var string
     */
    private $path = './runtime/data/';

    /**
     * 复写获取单一实例方法
     * @param string $dir
     * @return mixed
     */
    public static function instance($path = '')
    {
        $ident = md5($path);
        if (!isset(self::$instance[$ident])) {
            self::$instance[$ident] = new self($path);
        }
        return self::$instance[$ident];
    }

    /**
     * 进行一些初始化操作
     * File constructor.
     * @param string $dir
     */
    private function __construct($path = '')
    {
        if ($path) {
            $this->path = $path;
        }
    }

    /**
     * 设置缓存
     * @param string $key
     * @param string $value
     * @param int $expire
     * @return bool
     */
    public function set($key, $value, $expire = 10)
    {
        $this->createCachePath();
        $filename = $this->getFileName($key);
        $cacheArray = [
            'create' => time(),
            'expire' => $expire,
            'value' => $value
        ];
        $content = serialize($cacheArray);
        if (file_put_contents($filename, $content)) {
            $this->files[$key] = $cacheArray;
            return true;
        }
        return false;
    }

    /**
     * 获取缓存
     * @param string $key
     * @param null $callable
     * @return bool|false|string
     */
    public function get($key, $callable = null)
    {
        // 判断缓存是否存在
        if ($this->exists($key)) {
            // 返回缓存数据
            return $this->getCacheContent($key);
        } elseif (is_callable($callable)) {
            // 如果缓存不存在但是存在callable，则调用
            return $callable($this);
        }
        return false;
    }

    /**
     * 删除缓存
     * @param string $key
     * @return bool
     */
    public function remove($key)
    {
        $filename = $this->getFileName($key);
        if (is_file($filename)) {
            @unlink($filename);
        }
        return true;
    }

    /**
     * 判断缓存是否存在/有效
     * @param string $key
     * @return bool
     */
    public function exists($key)
    {
        $filename = $this->getFileName($key);
        if (is_file($filename) && !$this->expire($key)) {
            return true;
        }
        return false;
    }

    /**
     * 获取文件缓存内容
     * @param string $key
     * @return false|string
     */
    private function getCacheContent($key)
    {
        $content = $this->readCacheFile($key);
        return (!$content) ? false : $content['value'];
    }

    /**
     * 判断缓存是否过期
     * @param string $key
     * @return bool
     */
    private function expire($key)
    {
        $content = $this->readCacheFile($key);
        // 缓存文件存在，已读取到内容
        if (!empty($content)) {
            $mtime = $content['create'];
            $expire = $content['expire'];
            if ($expire == 0) {
                return false;
            } elseif ((time() - $mtime >= $expire)) {
                // 已超时，删除缓存
                $this->remove($key);
                return true;
            } else {
                return false;
            }
        }
        // 否则直接返回超时
        return true;
    }

    /**
     * 读取缓存文件
     * @param string $key
     * @return mixed
     */
    private function readCacheFile($key)
    {
        if (!isset($this->files[$key])) {
            // 获取文件名
            $filename = $this->getFileName($key);
            if (is_file($filename)) {
                $content = file_get_contents($filename);
                $this->files[$key] = unserialize($content);
                unset($content);
            } else {
                // 文件不存在
                $this->files[$key] = false;
            }
        }
        return $this->files[$key];
    }

    /**
     * 获取缓存文件名称
     * @param $key
     * @return string
     */
    public function getFileName($key)
    {
        return $this->path . $key . '.txt';
    }

    /**
     * 创建缓存目录
     */
    private function createCachePath()
    {
        if (!is_dir($this->path)) {
            mkdir($this->path, 0755, true);
        }
    }

}
