<?php

namespace top\library\cache\ifs;

interface CacheIfs
{

    public function set($key, $value, $expire = 0);

    public function get($key, $callable = null);

    public function remove($key);

    public function exists($key);

}
