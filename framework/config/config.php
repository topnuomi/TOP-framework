<?php
// 默认配置

return [
    'default_controller' => 'Index',
    'default_method' => 'index',
    'compel_route' => false,
    'complete_parameter' => true,
    'error_pages' => [
        404 => './404.html',
    ],
    'middleware' => [
        \top\middleware\Action::class,
        \top\middleware\View::class,
    ],
    'session' => [
        'open' => false,
        'prefix' => '',
    ],
    'cache' => [
        'driver' => 'File',
        'path' => './runtime/cache/',
    ],
    'db' => [
        'driver' => 'Mysql',
        'host' => '127.0.0.1',
        'user' => '',
        'passwd' => '',
        'dbname' => '',
        'prefix' => '',
        'port' => 3306,
        'charset' => 'utf8'
    ],
    'redis' => [
        'host' => '127.0.0.1',
        'port' => 6379,
        'auth' => '',
    ],
    'view' => [
        'driver' => 'Top',
        'tagLib' => [],
        'ext' => 'html',
        'path' => APP_PATH . CURRENT_MODULE . '/view/',
        'cache_path' => './runtime/cache/application/' . CURRENT_MODULE . '/',
        'compile_path' => './runtime/compile/application/' . CURRENT_MODULE . '/',
        'left' => '<',
        'right' => '>',
        'expire' => 5
    ],
];
